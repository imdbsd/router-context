import React from 'react';
import { Link } from 'react-router-dom';
import Locale from './Locale';

class Layout extends React.Component {
  state = {
      isToggled: true
  };
  render() {
    return (
      <div className="container">
        <nav className="navbar" aria-label="main navigation">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              <img
                src="https://bulma.io/images/bulma-logo.png"
                alt="Bulma: a modern CSS framework based on Flexbox"
                width={112}
                height={28}
              />
            </Link>
            <a
              role="button"
              className="navbar-burger"
              aria-label="menu"
              aria-expanded="false"
            >
              <span aria-hidden="true" />
              <span aria-hidden="true" />
              <span aria-hidden="true" />
            </a>
          </div>
          <div className="navbar-end">
            <Link className="navbar-item" to="/">
              Home
            </Link>
            <Locale/>
          </div>
        </nav>
        {this.props.children}
      </div>
    );
  }
}

export default Layout;
