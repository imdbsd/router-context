import React, { Component } from 'react';

export default class Locale extends Component {
    state = {
        isToggled: false
    }

    handleToggle = isToggled => {
        this.setState({isToggled});
    }
  render() {
    return (
      <div className={`dropdown navbar-item ${this.state.isToggled ? 'is-active' : ''}`}>
        <div className="dropdown-trigger">
          <button
            className="button"
            aria-haspopup="true"
            aria-controls="dropdown-menu"
            onClick={() => this.handleToggle(!this.state.isToggled)}
          >
            <span>en</span>
            <span className="icon is-small">
              <i className="fas fa-angle-down" aria-hidden="true" />
            </span>
          </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
          <div className="dropdown-content">
            <a href="#" className="dropdown-item is-active">
              en
            </a>
            <a href="#" className="dropdown-item ">
              id
            </a>
          </div>
        </div>
      </div>
    );
  }
}
