import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Product from './pages/Product';
import ProductList from './pages/ProductList'


class AppRouter  extends Component {
    render() {
        return (
            <BrowserRouter>
                <Fragment>
                    <Route path="/" exact component={ProductList} />
                    <Route path="/products" component={ProductList} />
                    <Route path="/product/:slug" component={Product} />
                </Fragment>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<AppRouter />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
